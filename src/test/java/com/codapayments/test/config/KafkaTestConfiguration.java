package com.codapayments.test.config;

import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@TestConfiguration
public class KafkaTestConfiguration {

    @Bean
    @Primary
    public SchemaRegistryClient mockSchemaRegistryClient() {
        return new MockSchemaRegistryClient();
    }
}
