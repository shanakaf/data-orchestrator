package com.codapayments.test.service;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.converter.ConverterFactory;
import com.codapayments.test.converter.ConverterType;
import com.codapayments.test.converter.JsonConverter;
import com.codapayments.test.dto.CustomerDetailsJsonFormat;
import com.codapayments.test.exception.InvalidConverterTypeException;
import com.codapayments.test.exception.InvalidWriterTypeException;
import com.codapayments.test.writer.JsonDiskFileWriter;
import com.codapayments.test.writer.WriterFactory;
import com.codapayments.test.writer.WriterType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RecordProcessServiceTest {

    private RecordProcessService service;

    @Mock
    private ConverterFactory converterFactory;

    @Mock
    private WriterFactory writerFactory;

    @Mock
    private JsonConverter jsonConverter;

    @Mock
    private JsonDiskFileWriter jsonWriter;

    @Before
    public void setUp() {
        service = new RecordProcessService(converterFactory, writerFactory);
    }

    @Test(expected = InvalidConverterTypeException.class)
    public void givenInvalidConverterType_WhenConvert_ThenThrowInvalidConverterTypeException() throws Exception {
        Message message = MessageBuilder.withPayload(new CustomerFileFormat()).build();
        when(converterFactory.getConverterByType(null)).thenThrow(InvalidConverterTypeException.class);

        service.process(message, null, WriterType.DISK_WRITER);

        verifyNoInteractions(jsonConverter.convert(any()));
        verifyNoInteractions(writerFactory.getRecordWriterByType(any()));
        verify(jsonWriter, times(0)).write(any());
    }

    @Test(expected = InvalidWriterTypeException.class)
    public void givenInvalidWriteType_WhenConvert_ThenThrowInvalidWriteTypeException() throws Exception {
        Message message = MessageBuilder.withPayload(new CustomerFileFormat()).build();
        CustomerDetailsJsonFormat formattedMessage = CustomerDetailsJsonFormat.builder().build();

        when(converterFactory.getConverterByType(ConverterType.JSON_CONVERTER)).thenReturn(jsonConverter);
        when(jsonConverter.convert((CustomerFileFormat) message.getPayload())).thenReturn(formattedMessage);
        when(writerFactory.getRecordWriterByType(null)).thenThrow(InvalidWriterTypeException.class);

        service.process(message, ConverterType.JSON_CONVERTER, null);

        verify(jsonWriter, times(0)).write(any());
    }

    @Test
    public void givenValidParameters_WhenConvert_ThenExecuteWrite() throws Exception {
        Message message = MessageBuilder.withPayload(new CustomerFileFormat()).build();
        CustomerDetailsJsonFormat formattedMessage = CustomerDetailsJsonFormat.builder().build();

        when(converterFactory.getConverterByType(ConverterType.JSON_CONVERTER)).thenReturn(jsonConverter);
        when(jsonConverter.convert((CustomerFileFormat) message.getPayload())).thenReturn(formattedMessage);
        when(writerFactory.getRecordWriterByType(WriterType.DISK_WRITER)).thenReturn(jsonWriter);

        service.process(message, ConverterType.JSON_CONVERTER, WriterType.DISK_WRITER);

        verify(jsonWriter, times(1)).write(formattedMessage);
    }

}