package com.codapayments.test.kafka.consumer;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.avro.ValueType;
import com.codapayments.test.converter.ConverterType;
import com.codapayments.test.service.RecordProcessService;
import com.codapayments.test.writer.WriterType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerDetailsConsumerTest {

    private CustomerDetailsConsumer consumer;

    @Mock
    private RecordProcessService recordProcessService;

    @Mock
    private Acknowledgment acknowledgment;

    @Captor
    private ArgumentCaptor<Message> requestCaptor;

    @Before
    public void setUp() {
        consumer = new CustomerDetailsConsumer(recordProcessService);
    }

    @Test
    public void givenMessages_WhenSupportedMessage_ThenAcknowledgeAll() throws Exception {
        List<Message> messages = newArrayList(
                MessageBuilder.withPayload(new CustomerFileFormat()).build(),
                MessageBuilder.withPayload(new CustomerFileFormat()).build()
        );
        consumer.onCustomerDetailsBatch(messages, acknowledgment);

        InOrder inOrder = inOrder(recordProcessService, acknowledgment);
        inOrder.verify(recordProcessService, times(2)).process(requestCaptor.capture(), eq(ConverterType.JSON_CONVERTER), eq(WriterType.DISK_WRITER));
        inOrder.verify(acknowledgment).acknowledge();

        assertThat(requestCaptor.getAllValues()).hasSize(2);

    }

    @Test
    public void givenMessages_WhenUnsupportedMessagesAreAdded_ThenIgnoreThem() throws Exception {
        List<Message> messages = newArrayList(
                MessageBuilder.withPayload(new CustomerFileFormat()).build(),
                MessageBuilder.withPayload(new ValueType()).build()
        );
        consumer.onCustomerDetailsBatch(messages, acknowledgment);

        InOrder inOrder = inOrder(recordProcessService, acknowledgment);
        inOrder.verify(recordProcessService, times(1)).process(requestCaptor.capture(), eq(ConverterType.JSON_CONVERTER), eq(WriterType.DISK_WRITER));
        inOrder.verify(acknowledgment).acknowledge();

        assertThat(requestCaptor.getAllValues()).hasSize(1);

    }

    @Test
    public void givenMessages_WhenExceptionOccurs_ThenAcknowledgeAll() throws Exception {
        List<Message> messages = newArrayList(
                MessageBuilder.withPayload(new CustomerFileFormat()).build(),
                MessageBuilder.withPayload(new CustomerFileFormat()).build()
        );
        consumer.onCustomerDetailsBatch(messages, acknowledgment);
        lenient().doThrow(new Exception()).when(recordProcessService).process(any(), any(), any());
        verify(acknowledgment).acknowledge();
    }

}