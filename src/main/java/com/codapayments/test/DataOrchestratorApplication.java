package com.codapayments.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataOrchestratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataOrchestratorApplication.class, args);
    }

}
