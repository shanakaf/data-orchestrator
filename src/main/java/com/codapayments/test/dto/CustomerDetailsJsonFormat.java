package com.codapayments.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
@JsonPropertyOrder({"firstName", "lastName", "companyName", "address", "city", "country", "state", "zip", "phoneNumberOne", "phoneNumberTwo", "emailAddress", "website"})
public class CustomerDetailsJsonFormat {

    @JsonProperty("first_name")
    ValueType firstName;
    @JsonProperty("last_name")
    ValueType lastName;
    @JsonProperty("company_name")
    ValueType companyName;
    ValueType address;
    ValueType city;
    ValueType country;
    ValueType state;
    ValueType zip;
    @JsonProperty("phone1")
    ValueType phoneNumberOne;
    @JsonProperty("phone2")
    ValueType phoneNumberTwo;
    @JsonProperty("email")
    ValueType emailAddress;
    @JsonProperty("web")
    ValueType website;
}
