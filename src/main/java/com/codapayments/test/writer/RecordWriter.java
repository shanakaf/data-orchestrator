package com.codapayments.test.writer;

public interface RecordWriter<I> {
    void write(I record) throws Exception;
}
