package com.codapayments.test.writer;

import com.codapayments.test.exception.InvalidWriterTypeException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WriterFactory<I> {

    private final JsonDiskFileWriter jsonDiskFileWriter;

    public RecordWriter getRecordWriterByType(WriterType writerType) {
        if (writerType == WriterType.DISK_WRITER) {
            return jsonDiskFileWriter;
        }
        throw new InvalidWriterTypeException(Optional.ofNullable(writerType).get().name());
    }

}
