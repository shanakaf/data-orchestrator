package com.codapayments.test.writer;

import com.codapayments.test.dto.CustomerDetailsJsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.util.UuidUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Slf4j
public class JsonDiskFileWriter implements RecordWriter<CustomerDetailsJsonFormat> {

    @Value("${codapayment.disk-write.file.location}")
    private String filePath;

    @Override
    public void write(CustomerDetailsJsonFormat record) throws Exception {
        File file = new File(filePath + UuidUtil.getTimeBasedUuid() + ".json");
        new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(file, record);
        log.info("Successfully write the file {} of user {} ", file.getAbsolutePath(), record.getFirstName());
    }
}
