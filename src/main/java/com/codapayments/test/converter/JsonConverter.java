package com.codapayments.test.converter;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.dto.CustomerDetailsJsonFormat;
import com.codapayments.test.dto.ValueType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JsonConverter implements RecordsConverter<CustomerFileFormat, CustomerDetailsJsonFormat> {

    @Override
    public CustomerDetailsJsonFormat convert(CustomerFileFormat originalRecord) throws Exception {
        log.info("Converting record to JSON format of user : {}", originalRecord.getFirstName());
        return CustomerDetailsJsonFormat.builder()
                .firstName(ValueType.builder()
                        .type(originalRecord.getFirstName().getType())
                        .value(originalRecord.getFirstName().getValue())
                        .build())
                .lastName(ValueType.builder()
                        .type(originalRecord.getLastName().getType())
                        .value(originalRecord.getLastName().getValue())
                        .build())
                .companyName(ValueType.builder()
                        .type(originalRecord.getCompanyName().getType())
                        .value(originalRecord.getCompanyName().getValue())
                        .build())
                .address(ValueType.builder()
                        .type(originalRecord.getAddress().getType())
                        .value(originalRecord.getAddress().getValue())
                        .build())
                .city(ValueType.builder()
                        .type(originalRecord.getCity().getType())
                        .value(originalRecord.getCity().getValue())
                        .build())
                .country(ValueType.builder()
                        .type(originalRecord.getCountry().getType())
                        .value(originalRecord.getCountry().getValue())
                        .build())
                .state(ValueType.builder()
                        .type(originalRecord.getState().getType())
                        .value(originalRecord.getState().getValue())
                        .build())
                .zip(ValueType.builder()
                        .type(originalRecord.getZip().getType())
                        .value(originalRecord.getZip().getValue())
                        .build())
                .phoneNumberOne(ValueType.builder()
                        .type(originalRecord.getPhoneNumberOne().getType())
                        .value(originalRecord.getPhoneNumberOne().getValue())
                        .build())
                .phoneNumberTwo(ValueType.builder()
                        .type(originalRecord.getPhoneNumberTwo().getType())
                        .value(originalRecord.getPhoneNumberTwo().getValue())
                        .build())
                .emailAddress(ValueType.builder()
                        .type(originalRecord.getEmailAddress().getType())
                        .value(originalRecord.getEmailAddress().getValue())
                        .build())
                .website(ValueType.builder()
                        .type(originalRecord.getWebsite().getType())
                        .value(originalRecord.getWebsite().getValue())
                        .build())
                .build();
    }
}
