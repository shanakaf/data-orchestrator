package com.codapayments.test.converter;

import com.codapayments.test.exception.InvalidConverterTypeException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
@Component
@Slf4j
public class ConverterFactory {

    private final JsonConverter jsonConverter;

    public RecordsConverter getConverterByType(ConverterType converterType) {
        if (converterType == ConverterType.JSON_CONVERTER) {
            return jsonConverter;
        }
        throw new InvalidConverterTypeException(Optional.ofNullable(converterType).get().name());
    }
}
