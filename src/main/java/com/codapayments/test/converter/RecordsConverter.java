package com.codapayments.test.converter;

public interface RecordsConverter<I, O> {
    O convert(I originalRecord) throws Exception;
}
