package com.codapayments.test.service;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.converter.ConverterFactory;
import com.codapayments.test.converter.ConverterType;
import com.codapayments.test.converter.RecordsConverter;
import com.codapayments.test.dto.CustomerDetailsJsonFormat;
import com.codapayments.test.writer.RecordWriter;
import com.codapayments.test.writer.WriterFactory;
import com.codapayments.test.writer.WriterType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RecordProcessService {

    private final ConverterFactory converterFactory;
    private final WriterFactory writerFactory;

    public void process(Message message, ConverterType converterType, WriterType writerType) throws Exception {
        RecordsConverter<CustomerFileFormat, CustomerDetailsJsonFormat> converter = converterFactory.getConverterByType(converterType);
        CustomerDetailsJsonFormat formattedOutput = converter.convert((CustomerFileFormat) message.getPayload());
        log.info("Started processing file for user {} ", formattedOutput.getFirstName());
        RecordWriter<CustomerDetailsJsonFormat> writer = writerFactory.getRecordWriterByType(writerType);
        writer.write(formattedOutput);
    }
}
