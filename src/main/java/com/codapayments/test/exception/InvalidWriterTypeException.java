package com.codapayments.test.exception;

public class InvalidWriterTypeException extends RuntimeException {
    public InvalidWriterTypeException(String type) {
        super("Writer Type :" + type + " is not supported");
    }
}
