package com.codapayments.test.exception;

public class InvalidConverterTypeException extends RuntimeException {
    public InvalidConverterTypeException(String type) {
        super("Converter Type :" + type + " is not supported");
    }
}
