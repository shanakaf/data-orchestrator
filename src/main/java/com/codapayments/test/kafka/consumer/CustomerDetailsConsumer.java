package com.codapayments.test.kafka.consumer;

import com.codapayments.test.converter.ConverterType;
import com.codapayments.test.service.RecordProcessService;
import com.codapayments.test.util.MessageUtils;
import com.codapayments.test.writer.WriterType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
public class CustomerDetailsConsumer {

    private final RecordProcessService recordProcessService;

    @KafkaListener(topics = {"${codapayment.topic.customer-details.name}"})
    public void onCustomerDetailsBatch(List<Message> messages, Acknowledgment acknowledgment) {
        log.info("Fetched message batch of size {} ", messages.size());
        List<Message> supportedMessages = messages.stream().filter(MessageUtils::isSupportedKafkaMessage).collect(Collectors.toList());
        log.info("Number of supported messages {} ", supportedMessages.size());
        supportedMessages.forEach(message -> {
            try {
                recordProcessService.process(message, ConverterType.JSON_CONVERTER, WriterType.DISK_WRITER);
            } catch (Exception e) {
                log.error("Error in processing record {} ", message.getPayload(), e);
            }
        });
        acknowledgment.acknowledge();
    }
}
