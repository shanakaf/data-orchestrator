package com.codapayments.test.util;

import com.codapayments.test.avro.CustomerFileFormat;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.KafkaNull;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;
import org.springframework.messaging.Message;
import org.springframework.util.SerializationUtils;

@UtilityClass
@Slf4j
public class MessageUtils {

    public static boolean isSupportedKafkaMessage(Message message) {
        try {
            if (message.getPayload() instanceof CustomerFileFormat) {
                return true;
            }
            if (message.getPayload() instanceof KafkaNull) {
                log.error("Kafka message deserialization exception {} ", convertToKafkaDeserializationExceptionMessage(message));
            } else {
                log.error("Received un supported message type {}", message.getPayload().getClass());
            }
        } catch (RuntimeException e) {
            log.error("Exception in validating received message type ", e);
        }
        return false;
    }

    private static String convertToKafkaDeserializationExceptionMessage(Message message) {
        try {
            byte[] headerValue = (byte[]) message.getHeaders().get(ErrorHandlingDeserializer2.VALUE_DESERIALIZER_EXCEPTION_HEADER);
            return ((Exception) SerializationUtils.deserialize(headerValue)).getMessage();
        } catch (RuntimeException e) {
            return "Failed to read the deserialization cause : " + e.getMessage();
        }
    }

}
